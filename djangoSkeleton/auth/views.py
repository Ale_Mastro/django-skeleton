import json
import requests
from authlib.integrations.django_client import OAuth
from django.conf import settings
from django.shortcuts import redirect, render, redirect
from django.urls import reverse
from urllib.parse import quote_plus, urlencode
from django.http import JsonResponse
# oauth = OAuth()

# oauth.register(
#     "auth0",
#     client_id=settings.AUTH0_CLIENT_ID,
#     client_secret=settings.AUTH0_CLIENT_SECRET,
#     client_kwargs={
#         "scope": "openid profile email",
#     },
#     server_metadata_url=f"https://{settings.AUTH0_DOMAIN}/.well-known/openid-configuration",
# )

def login(request):
    auth0_domain = settings.AUTH0_DOMAIN
    auth0_client_id = settings.AUTH0_CLIENT_ID
    redirect_uri = "http://localhost:8000/oauth/callback"  # Replace with your React app's callback URL

    auth0_login_url = f"https://{auth0_domain}/authorize" \
                      f"?client_id={auth0_client_id}" \
                      f"&response_type=code" \
                      f"&redirect_uri={redirect_uri}" \
                      f"&scope=openid profile email offline_access"  # Customize scopes as needed

    return JsonResponse({"auth0_login_url": auth0_login_url})

def refresh_token(request):
    token_refresh_url = f'https://{settings.AUTH0_DOMAIN}/oauth/token'
    token_refresh_data = {
        'grant_type': 'refresh_token',
        'client_id': settings.AUTH0_CLIENT_ID,
        'client_secret': settings.AUTH0_CLIENT_SECRET,
        'refresh_token': request.GET.get('refresh_token')
    }

    # Make a POST request to refresh the access token
    response = requests.post(token_refresh_url, data=token_refresh_data)

    # Parse the response JSON
    token_response = response.json()

    # Check if the response contains a new access token
    if 'access_token' in token_response:
        new_access_token = token_response['access_token']
        return JsonResponse({"access_token": new_access_token})

    else:
        # Handle the error (e.g., refresh token is invalid)
        error_message = token_response.get('error_description', 'Unknown error')
        print('Error refreshing token:', error_message)

def callback(request):
    # print("aaaaaaa")
    print(request.GET.get('code'))


    # Make a POST request to Auth0's token endpoint to exchange the code for a token
    token_endpoint = f"https://{settings.AUTH0_DOMAIN}/oauth/token"
    data = {
        "grant_type": "authorization_code",
        "client_id": settings.AUTH0_CLIENT_ID,
        "client_secret": settings.AUTH0_CLIENT_SECRET,
        "code": request.GET.get('code'),
        "redirect_uri": "http://localhost:8000/callback",  # Should match your Auth0 application's settings
    }

    response = requests.post(token_endpoint, data=data)
    # print(response.error)
    if response.status_code == 200:
        token_data = response.json()
        print(token_data)
        access_token = token_data.get('access_token')
        refresh_token = token_data.get('refresh_token')
        # You can store or use the access_token as needed
        redir = redirect('http://localhost:5173/')
        redir.set_cookie('access_token', access_token)
        redir.set_cookie('refresh_token', refresh_token)

        return redir
    else:
        return JsonResponse({"error": "Failed to exchange code for token"}, status=400)
    # token = oauth.auth0.authorize_access_token(request)
    # request.session["user"] = token
    # return redirect()
    # redir = redirect('http://localhost:5173/')
    # redir.headers['headers'] = username.encode_auth_token()
    # return redir

def logout(request):
    request.session.clear()

    return redirect(
        f"https://{settings.AUTH0_DOMAIN}/v2/logout?"
        + urlencode(
            {
                "returnTo": request.build_absolute_uri(reverse("index")),
                "client_id": settings.AUTH0_CLIENT_ID,
            },
            quote_via=quote_plus,
        ),
    )

def index(request):
    response = redirect(
        f"{settings.FE_DOMAIN}",
    )

    response.set_cookie(
        key='access_token',
        expires=request.session.get("user").get('expires_at'),
        value=request.session.get("user").get('access_token'),
        max_age=request.session.get("user").get('expires_in')
    )
    response.set_cookie(
        key='id_token',
        expires=request.session.get("user").get('expires_at'),
        value=request.session.get("user").get('id_token'),
        max_age=request.session.get("user").get('expires_in')
    )
    return response