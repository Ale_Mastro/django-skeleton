from django.urls import path
from . import views 
    
urlpatterns = [
        path("", views.index, name="index"),
        path("login/", views.login, name="login"),
        path("refresh_token/", views.refresh_token, name="refresh token"),
        path("logout/", views.logout, name="logout"),
        path("callback/", views.callback, name="callback"),
]     